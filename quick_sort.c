/**
 * Nama / NIM : Bervianto Leo P / 13514047
 * Nama / NIM : Faza Thirafi / 13514033
 * */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

void swap(int *a, long lo, long hi) {
	int temp = a[lo];
	a[lo] = a[hi];
	a[hi] = temp;
}

void quicksort_2(int *a, long lo, long hi) {
	long i, div;
	if (lo < hi) {
		int x = a[lo];
		div = lo;
		for (i = lo+1; i < hi; i++) {
			if (a[i] <= x) {
				div++;
				swap(a, div, i);
			}
		}
		swap(a, lo, div);
		#pragma omp task firstprivate(a, lo, div)
		quicksort_2(a, lo, div);
		#pragma omp task firstprivate(a, div, hi)
		quicksort_2(a, div+1, hi);
	}
}

void quicksort_seq(int *a, long lo, long hi) {
	long i, div;
	if (lo < hi) {
		int x = a[lo];
		div = lo;
		for (i = lo+1; i < hi; i++) {
			if (a[i] <= x) {
				div++;
				swap(a, div, i);
			}
		}
		swap(a, lo, div);
		quicksort_seq(a, lo, div);
		quicksort_seq(a, div+1, hi);
	}
}

void print_arr(int *a, long n) {
	int i = 0;
	for(i = 0;i < n; i++){
        printf("%d ", a[i]);
    }
    printf("\n");
}

void validate_sort(int * data, int n) {
	int i;
	for (i=0; i < n - 1; i++) {
		if (data[i] > data[i+1]) {
			printf("Array tidak terurut.\n");
			break;
		}
	}
	printf("Array terurut.\n");
}

int main(int argc, char *argv[])
{
    if (argc!=3) {
		printf("Usage : quick_sort [number of thread] [number of array]");
	} else {
		int thread_count = atoi(argv[1]);
		int n = atoi(argv[2]);
		
		int a[n];
		int b[n];
		int i, temp;
		clock_t start_t, end_t;
		double total_t;
		
		omp_set_dynamic(0);
		omp_set_num_threads(thread_count);
		
		// Init random array
		srand(0);
		for (i=0; i < n; i++) {
			temp = rand() % 500000;
			a[i] = temp;
			b[i] = temp;
			
		}
				
		double start_time = omp_get_wtime();
		#pragma omp parallel
		{
			#pragma omp single nowait
				quicksort_2(a, 0, n);
		}
		double time = omp_get_wtime() - start_time;
		
		validate_sort(a,n);
		printf("Waktu eksekusi dengan paralel : %f sekon\n", time);

		start_t = clock();
		quicksort_seq(b,0,n);
		end_t = clock();
		
		total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
		validate_sort(b,n);
		printf("Waktu eksekusi tanpa paralel atau sequensial : %f sekon\n", total_t);
	}
    return 0;
}
